<?php


class promo {

    public static function update($file = array(), $sub_title, $title, $link, $status) {
        global $db;

        if ($file['size'] > 0) {

            $image = $db->single("SELECT image FROM promo WHERE id = 1");

            upload::remove($image, config::baseUploadPromoUrl());

            $upload = upload::add($file, config::baseUploadPromoUrl(), true);
            $image = $upload['file'];

            $db->query("UPDATE promo SET image = :image WHERE id = 1", array('image' => $image));

        }

        $update = $db->query("UPDATE promo SET sub_title = :sub_title, title = :title, link = :link, status = :status WHERE id = 1", array(
            'sub_title' => $sub_title,
            'title' => $title,
            'link' => $link,
            'status' => $status
        ));

        if ($update) {
            respond::alert('success', '', 'Promo successfully updated');
        }else {
            respond::alert('danger', '', 'Unable to update promo');
        }

    }// UPDATE PROMO

    public static function get() {
        global $db;

        $promo = $db->query("SELECT * FROM promo WHERE id = 1", array(), false);

        if ($promo) {
            return $promo;
        }else {
            return false;
        }

    }

}