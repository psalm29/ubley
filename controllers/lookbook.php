<?php


class lookbook {

    public static function all() {
        global $db;

        $lookbook = $db->query("SELECT * FROM lookbook ORDER BY timestamp DESC");

        if (count($lookbook) > 0) {
            return $lookbook;
        }else {
            return false;
        }

    }// GET ALL LOOKBOOK

    public static function edit($id, $title, $description, $file = array()) {
        global $db;

        if ($file['size'] > 0) {

            $image = $db->single("SELECT cover FROM lookbook WHERE id = :id", array('id' => $id));

            upload::remove($image, config::baseUploadLookbookCoverUrl());

            $upload = upload::add($file, config::baseUploadLookbookCoverUrl(), true);
            $image = $upload['file'];

            $db->query("UPDATE lookbook SET cover = :image WHERE id = :id", array('image' => $image, 'id' => $id));

        }

        $update = $db->query("UPDATE lookbook SET title = :title, description = :description WHERE id = :id", array(
            'id' => $id,
            'title' => $title,
            'description' => $description
        ));

        if ($update) {
            respond::alert('success', '', 'Lookbook successfully updated');
        }else {
            respond::alert('danger', '', 'Unable to update lookbook');
        }

    }// Edit lookbook

    public static function remove($id) {
        global $db;

        $cover = $db->single("SELECT cover FROM lookbook WHERE id = :id", array('id' => $id));

        upload::remove($cover, config::baseUploadLookbookCoverUrl());

        $remove = $db->query("DELETE FROM lookbook WHERE id = :id", array('id' => $id));

        if ($remove) {
            $images = self::allImages($id);
            if ($images) {
                foreach ($images as $image) {
                    self::removeImagesSilent($image['id']);
                }
                $db->query("DELETE FROM lookbook_images WHERE lookbook_id = :id", array('id' => $id));
            }
            respond::alert('success', '', 'Lookbook successfully removed');
        }else {
            respond::alert('danger', '', 'Unable to remove this lookbook');
        }

    }// Remove lookbook

    public static function add($title, $description, $file = array()) {
        global $db;

        $check = $db->query("SELECT title FROM lookbook WHERE title = :title", array('title' => $title), false);

        if ($check) {
            respond::alert('warning', '', 'Lookbook with the same title already exist');
            return false;
        }

        $upload = upload::add($file, config::baseUploadLookbookCoverUrl(), true);
        $image = $upload['file'];

        $add = $db->query("INSERT INTO lookbook (title, cover, description, timestamp) VALUES (:title, :image, :description, :now)", array(
            'title' => $title,
            'image' => $image,
            'description' => $description,
            'now' => time()
        ));

        if ($add) {
            respond::alert('success', '', 'Lookbook successfully added');
        }else {
            respond::alert('danger', '', 'Sorry, there was an error adding lookbook');
        }

    }// Add new lookbook

    public static function addImages($id, $files = array()) {
        global $db;

        $status = false;


        foreach ($files["tmp_name"] as $file) {

            $upload = upload::multiple($file, config::baseUploadLookbookUrl(), true);
            $image = $upload['file'];

            if ($db->query("INSERT INTO lookbook_images (lookbook_id, image) VALUES (:id, :image)", array('id' => $id, 'image' => $image))) {
                $status = true;
            }else {
                $status = false;
            }

        }

        if ($status) {
            respond::alert('success', '', 'Images successfully added to lookbook');
        }else {
            respond::alert('warning', '', 'Unable to add all images to lookbook');
        }

    }// Add images to lookbook

    public static function allImages($id) {
        global $db;

        $images = $db->query("SELECT * FROM lookbook_images WHERE lookbook_id = :id ORDER BY id DESC", array('id' => $id));

        if (count($images) > 0) {
            return $images;
        }else {
            return false;
        }

    }// Get all images in lookbook

    public static function removeImages($id) {
        global $db;

        $image = $db->single("SELECT image FROM lookbook_images WHERE id = :id", array('id' => $id));

        upload::remove($image, config::baseUploadLookbookUrl());

        $remove = $db->query("DELETE FROM lookbook_images WHERE id = :id", array('id' => $id));

        if ($remove) {
            respond::alert('success', '', 'Image successfully removed from lookbook');
        }else {
            respond::alert('danger', '', 'Unable to remove this image from lookbook');
        }

    }

    public static function removeImagesSilent($id) {
        global $db;

        $image = $db->single("SELECT image FROM lookbook_images WHERE id = :id", array('id' => $id));

        upload::remove($image, config::baseUploadLookbookUrl());

    }

}