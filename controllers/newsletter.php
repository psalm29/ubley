<?php


class newsletter {

    public static function add($email) {
        global $db;

        if (self::check($email)) {
            respond::alert('warning', 'Email address already added!', $email.' has already been added to our newsletter');
            return false;
        }

        $insert = $db->query("INSERT INTO newsletter (email, timestamp) VALUES (:email, :now)", array(
            'email' => $email,
            'now' => time()
        ));

        if ($insert) {
            respond::alert('success', 'Thank you for joining our newsletter!', $email.' has been added to our newsletter');
        }else {
            respond::alert('danger', '', 'Unable to add you to our newsletter at the moment');
        }

    }

    public static function check($email) {
        global $db;

        $check = $db->single("SELECT email FROM newsletter WHERE email = :email", array('email' => $email));

        if ($check) {
            return true;
        }else {
            return false;
        }

    }

}