<?php


class order {

    public static function details($order_id) {
        global $db;

        $items = $db->query("SELECT order_details.*, thumbnail, slug, name FROM order_details LEFT JOIN products ON product_id = products.id WHERE order_id = :order_id", array('order_id' => $order_id));

    return $items;

    }

    public static function all() {
        global $db;

        $orders = $db->query("SELECT orders.*, fname, lname, email FROM orders LEFT JOIN accounts ON user_id = accounts.id ORDER BY orders.id DESC");

        if (count($orders) > 0) {
            return $orders;
        }else {
            return false;
        }

    }

    public static function remove($order_id) {
        global $db;

        $remove = $db->query("DELETE FROM orders WHERE order_id = :order_id", array('order_id' => $order_id));

        if ($remove) {
            respond::alert('success', '', 'Order has been successfully removed');
        }else {
            respond::alert('danger', '', 'Unable to remove order at the moment');
        }

    }


}