<?php


class staff {

    public static function all() {
        global $db;

        $staff = $db->query("SELECT * FROM staff WHERE role < 2 ORDER BY id DESC");

        if (count($staff) > 0) {
            return $staff;
        }else {
            return false;
        }

    }// FETCH ALL STAFF ACCOUNTS

    public static function edit($staff_id, $name, $email, $phone, $username, $address) {
        global $db;

        $check_email = $db->single("SELECT email FROM staff WHERE email = :email AND id != :staff_id", array('staff_id' => $staff_id, 'email' => $email));
        $check_username = $db->single("SELECT email FROM staff WHERE username = :username AND id != :staff_id", array('staff_id' => $staff_id, 'username' => $username));

        if ($check_email) {
            respond::alert('warning', '', 'Staff with the same email address already exist');
            return false;
        }

        if ($check_username) {
            respond::alert('warning', '', 'Staff with the same username already exist');
            return false;
        }

        $update = $db->query("UPDATE staff SET name = :name, email = :email, phone = :phone, address = :address, username = :username WHERE id = :staff_id", array(
            'staff_id' => $staff_id,
            'name' => $name,
            'address' => $address,
            'email' => $email,
            'phone' => $phone,
            'username' => $username
        ));

        if ($update) {
            respond::alert('success', '', 'Staff account successfully updated');
        }else {
            respond::alert('danger', '', 'Unable to update staff account');
        }

    }// EDIT STAFF ACCOUNT

    public static function remove($staff_id) {
        global $db;

        $remove = $db->query("DELETE FROM staff WHERE id = :staff_id", array('staff_id' => $staff_id));

        if ($remove) {
            respond::alert('success', '', 'Staff account successfully removed');
        }else {
            respond::alert('danger', '', 'Unable to remove staff account');
        }


    }// REMOVE STAFF ACCOUNT

    public static function add($username, $name, $email, $phone, $address, $password) {
        global $db;

        $status = self::check($email, $username);

        if ($status == 1) {
            respond::alert('warning', '', 'Staff with the same email address already exist');
            return false;
        }

        if ($status == 2) {
            respond::alert('warning', '', 'Staff with the same username already exist');
            return false;
        }

        $insert = $db->query("INSERT INTO staff (name, username, email, phone, address, password, timestamp) VALUES (:name, :username, :email, :phone, :address, :password, :now)", array(
            'name' => $name,
            'username' => $username,
            'email' => $email,
            'phone' => $phone,
            'address' => $address,
            'now' => time(),
            'password' => request::securePwd($password)
        ));

        if ($insert) {
            respond::alert('success', '', 'Staff account successfully created');
        }else {
            respond::alert('danger', '', 'Unable to create staff account');
        }

    }// ADD STAFF ACCOUNT


    public static function check($email, $username) {
        global $db;

        $staff_email = $db->single('SELECT email FROM staff WHERE email = :email', array('email' => $email));
        $staff_username = $db->single('SELECT username FROM staff WHERE username = :username', array('username' => $username));

        $status = 0;

        if ($email == $staff_email) {
            $status = 1;
        }

        if ($username == $staff_username) {
            $status = 2;
        }

        return $status;

    }// CHECK IF EMAIL EXIST

}