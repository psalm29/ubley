<?php

$pages = array("orders", "offers", "slider", "staff", "lookbook", "customers", "category", "login", "password", "logout", "mail", "products", "edit-product");

if ($routes[2] != 'admin') {

    if (in_array($routes[2], $pages)) {

        $page = $routes[2];

    } else {

        $page = 'products';

    }

} else {
    $page = 'products';
}

if ($page == 'login') {
    if (isset($_SESSION['logged_staff'])) {
        $page = 'product';
    }
    require('views/admin/'.$page.'.php');
} else{

    if (!isset($_SESSION['logged_staff'])) {
        $page = 'login';
    }else{
        $user = $_SESSION['logged_staff'];
        $account = $db->query("SELECT * FROM staff WHERE username = :user", array('user' => $user), false);

        if ($account) {
            $staff_id = $account['id'];
            $staff_role = $account['role'];
        }else{
            unset($_SESSION['logged_staff']);
            $page = 'login';
        }
    }

    if ($page == 'login') {
        require('views/admin/'.$page.'.php');
    }else {
        require('views/admin/partials/header.php');
        require('views/admin/'.$page.'.php');
        require('views/admin/partials/footer.php');
    }
}