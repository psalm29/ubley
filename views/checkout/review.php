<div class="col-lg-8">
    <?php
    require 'views/checkout/layout/menu.php';
    ?>
    <form action="checkout/confirmation" method="post"></form>
    <div class="row mb-5">
        <div class="cart">
            <div class="cart-wrapper">
                <div class="cart-header text-center">
                    <div class="row">
                        <div class="col-5">Item</div>
                        <div class="col-2">Price</div>
                        <div class="col-2">Quantity</div>
                        <div class="col-2">Total</div>
                        <div class="col-1"></div>
                    </div>
                </div>
                <div class="cart-body" ng-repeat="item in Shopcart | orderBy:'id'">
                    <!-- Product-->
                    <div class="cart-item">
                        <div class="row d-flex align-items-center text-center">
                            <div class="col-5">
                                <div class="d-flex align-items-center">
                                    <a href="shop/product/{{item.slug}}">
                                        <img src="<?php echo config::baseUploadProductUrl(); ?>{{item.pic}}" alt="{{item.name}}" class="cart-item-img">
                                    </a>
                                    <div class="cart-title text-left">
                                        <a href="shop/product/{{item.slug}}" class="text-uppercase text-dark"><strong>{{item.name}}</strong></a>
                                        <br>
                                        <span class="text-muted text-sm">Size: {{item.size}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-2">{{item.unit | currency:'₦':0}}</div>
                            <div class="col-2">
                                <div class="d-flex align-items-center">
                                    <div class="btn btn-items btn-items-decrease" ng-click="qtyUpdate(item.id, 'decrease')">-</div>
                                    <input type="text" value="{{item.qty}}" class="form-control text-center input-items">
                                    <div class="btn btn-items btn-items-increase" ng-click="qtyUpdate(item.id, 'increase')">+</div>
                                </div>
                            </div>
                            <div class="col-2 text-center">{{item.cost | currency:'₦':0}}</div>
                            <div class="col-1 text-center"><a href="javascript:;" class="cart-remove" ng-click="ejecter(item.id, 'false')"> <i class="fa fa-times"></i></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mb-5 d-flex justify-content-between flex-column flex-lg-row"><a href="checkout/payment" class="btn btn-link text-muted"> <i class="fa fa-angle-left mr-2"></i>Back to payment method</a>
        <button ng-click="order()" class="btn btn-dark">Place order<i class="fa fa-angle-right ml-2"></i></button>
    </div>
    </form>
</div>

<script src="https://js.paystack.co/v1/inline.js"></script>