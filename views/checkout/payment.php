<div class="col-lg-8">
    <?php
    require 'views/checkout/layout/menu.php';
    ?>

        <div class="mb-5"ng-controller="payment">
            <div id="accordion" role="tablist">
                <div class="block mb-3">
                    <div id="headingOne" role="tab" class="block-header"><strong><a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="accordion-link">Credit Card</a></strong></div>
                    <div id="collapseOne" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" class="collapse">
                        <div class="block-body py-5 d-flex align-items-center">
                            <input type="radio" class="cardPayment" name="shippping" ng-click="updatePayment('card')" value="card" id="payment-method-1">
                            <label for="payment-method-1" class="ml-3"><strong class="d-block text-uppercase mb-2"> Pay with Paystack</strong>
                                <span class="text-muted text-sm">Online payment will be processed using Paystack.</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="block mb-3">
                    <div id="headingTwo" role="tab" class="block-header"><strong><a data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" class="accordion-link collapsed">Bank Transfer</a></strong></div>
                    <div id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion" class="collapse">
                        <div class="block-body py-5 d-flex align-items-center">
                            <input type="radio" class="bankPayment" name="shippping" ng-click="updatePayment('bank')" value="bank" id="payment-method-2">
                            <label for="payment-method-2" class="ml-3"><strong class="d-block text-uppercase mb-2"> Transfer to corporate account</strong>
                                <?php echo config::accountDetails(); ?>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mb-5 d-flex justify-content-between flex-column flex-lg-row"><a href="checkout/delivery" class="btn btn-link text-muted"> <i class="fa fa-angle-left mr-2"></i>Back to delivery method</a>
            <a href="checkout/review" class="btn btn-dark">Continue to order review<i class="fa fa-angle-right ml-2"></i></a>
        </div>

</div>