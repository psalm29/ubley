
<section class="hero">
    <div class="container">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active">Order confirmed        </li>
        </ol>
        <!-- Hero Content-->
        <div class="hero-content pb-5 text-center">
            <h1 class="hero-heading">Order confirmed</h1>
        </div>
    </div>
</section>
<section class="pb-5" ng-controller="transaction">
    <div class="container text-center">
        <div class="icon-rounded bg-success mb-3 mx-auto text-white">
            <svg class="svg-icon w-2rem h-2rem align-middle">
                <use xlink:href="#checkmark-1"> </use>
            </svg>
        </div>
        <h4 class="mb-3 ff-base">Thank you, {{transaction.name}}. Your order is confirmed.</h4>
        <h4 class="mb-3 ff-base">Your order ID is {{transaction.order_id}}.</h4>
        <p class="text-muted mb-5">Your order hasn't been shipped yet but we will call you to confirm shipping fee.</p>
        <p>
        <form action="account/login" method="post">
            <button type="submit" name="orderUser" value="{{transaction.email}}" class="btn btn-outline-dark">View or manage your order</button>
        </form>
        </p>
    </div>
</section>
