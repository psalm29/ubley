<div class="col-lg-8">
    <?php

    require 'views/checkout/layout/menu.php';

    if (isset($_SESSION['logged_user'])) {
        $email= $_SESSION['logged_user'];
        $account = user::check($email);
    }

    ?>
    <form ng-submit="updateAddress(<?php
    if (isset($_SESSION['logged_user'])) {
        ?>
        '<?php echo $account['fname'].' '.$account['lname']; ?>', '<?php echo $account['email']; ?>', '<?php echo $account['phone']; ?>', '<?php echo $account['country']; ?>', '<?php echo $account['state']; ?>', '<?php echo $account['city']; ?>', '<?php echo $account['address']; ?>'
        <?php
    }else {
        echo 'name, email, phone, country, state, city, address';
    }
    ?>)">
        <div class="block">
            <!-- Invoice Address-->
            <div class="block-header">
                <h6 class="text-uppercase mb-0">Invoice address</h6>
            </div>
            <?php
                if (isset($_SESSION['logged_user'])) {
                    respond::alert('info', '', 'Update invoice address from account <a class="text-dark" href="account/address">address settings</a>');
                }
            ?>
            <div class="block-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="fullname_invoice" class="form-label">Full Name</label>
                        <input type="text" name="name" placeholder="Full Name" <?php if (isset($_SESSION['logged_user'])) {
                            ?>value="<?php echo $account['fname'].' '.$account['lname']; ?>" <?php
                        }else {
                            echo 'ng-model="name"';
                        } ?> id="fullname_invoice" required class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="emailaddress_invoice" class="form-label">Email Address</label>
                        <input type="text" name="email" <?php if (isset($_SESSION['logged_user'])) {
                        ?>value="<?php echo $account['email']; ?>" <?php
                               }else {
                            echo 'ng-model="email"';
                        } ?> placeholder="Email Address" required id="emailaddress_invoice" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="phonenumber_invoice" class="form-label">Phone Number</label>
                        <input type="text" name="phone" <?php if (isset($_SESSION['logged_user'])) {
                        ?>value="<?php echo $account['phone']; ?>" <?php
                               }else {
                            echo 'ng-model="phone"';
                        } ?> id="phonenumber_invoice" required placeholder="Phone Number" class="form-control">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="fullname_invoice" class="form-label">Country</label>
                        <input type="text" name="country" <?php if (isset($_SESSION['logged_user'])) {
                        ?>value="<?php echo $account['country']; ?>" <?php
                               }else {
                            echo 'ng-model="country"';
                        } ?> required placeholder="Country" id="fullname_invoice" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="state_invoice" class="form-label">State</label>
                        <input type="text" name="state" <?php if (isset($_SESSION['logged_user'])) {
                        ?>value="<?php echo $account['state']; ?>" <?php
                               }else {
                            echo 'ng-model="state"';
                        } ?> required placeholder="State" id="state_invoice" class="form-control">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="city_invoice" class="form-label">City</label>
                        <input type="text" name="city" <?php if (isset($_SESSION['logged_user'])) {
                        ?>value="<?php echo $account['city']; ?>" <?php
                        }else {
                            echo 'ng-model="city"';
                        } ?> required placeholder="City" id="city_invoice" class="form-control">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="address" class="form-label">Delivery Address</label>
                        <textarea name="address" id="address" <?php if (!isset($_SESSION['logged_user'])) {
                            echo 'ng-model="address"';
                        } ?> required rows="3" class="form-control"><?php if (isset($_SESSION['logged_user'])) {
                                 echo $account['address'];
                            } ?></textarea>
                    </div>
                </div>
                <!-- /Invoice Address-->
            </div>
        </div>
        <div class="mb-5 d-flex justify-content-between flex-column flex-lg-row"><a href="cart" class="btn btn-link text-muted"> <i class="fa fa-angle-left mr-2"></i>Back to cart</a><button class="btn btn-dark">Choose delivery method<i class="fa fa-angle-right ml-2"></i></button></div>
    </form>
</div>