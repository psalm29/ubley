<ul class="custom-nav nav nav-pills mb-5">
    <li class="nav-item w-25"><a href="checkout/address" class="nav-link text-sm <?php
if ($view == 'address') {
    echo 'active';
}else {
    echo 'disabled';
}
?>">Address</a></li>
    <li class="nav-item w-25"><a href="checkout/delivery" class="nav-link text-sm <?php
if ($view == 'delivery') {
    echo 'active';
}else {
    echo 'disabled';
}
?>">Delivery Method</a></li>
    <li class="nav-item w-25"><a href="checkout/payment" class="nav-link text-sm <?php
if ($view == 'payment') {
    echo 'active';
}else {
    echo 'disabled';
}
?>">Payment Method </a></li>
    <li class="nav-item w-25"><a href="checkout/review" class="nav-link text-sm <?php
if ($view == 'review') {
    echo 'active';
}else {
    echo 'disabled';
}
?>">Order Review</a></li>
</ul>