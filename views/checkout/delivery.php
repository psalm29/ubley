<div class="col-lg-8">
    <?php
    require 'views/checkout/layout/menu.php';
    ?>
    <div class="block my-5">
        <div class="block-body">
            <div class="row">
                <div class="col-md-12 mb-5">
                    <?php
                    respond::alert('info', 'We deliver to every location', 'Shipping is charged based on the location and made after placing your order');
                    ?>
                </div>
            </div>
        </div>
        <div class="mb-5 d-flex justify-content-between flex-column flex-lg-row"><a href="checkout/address" class="btn btn-link text-muted"> <i class="fa fa-angle-left mr-2"></i>Back to addresses</a><a href="checkout/payment" class="btn btn-dark">Choose payment method<i class="fa fa-angle-right ml-2"></i></a></div>
    </div>
</div>