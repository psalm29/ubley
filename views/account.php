<?php

$pages = array("orders", "profile", "address", "logout", "login");

if ($routes[2] != 'account') {

    if (in_array($routes[2], $pages)) {

        $page = $routes[2];

    } else {

        $page = 'orders';

    }

} else {
    $page = 'orders';
}

if ($page == 'login') {
    if (isset($_SESSION['logged_user'])) {
        header('location: ../account/orders');
    }
    require('views/account/'.$page.'.php');
} else{

    if (!isset($_SESSION['logged_user'])) {
        header('location: ../account/login');
    }else{
        $email= $_SESSION['logged_user'];
        $account = user::check($email);

        if (!$account) {
            unset($_SESSION['logged_user']);
            header('location: ../account/login');
        }
    }


    require('views/account/'.$page.'.php');

}