<?php

$pages = array("address", "payment", "delivery", "review", "confirmation");

if ($routes[2] != 'checkout') {

    if(in_array($routes[2], $pages)){

        $view = $routes[2];

    }else{

        header("location: ../checkout");

    }

}else {
    $view = 'address';
}

if ($view == 'confirmation') {
    require 'views/checkout/'.$view.'.php';
}else {
    ?>
<!-- Hero Section-->
<section class="hero" ng-init="checkCart()">
    <div class="container">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active">Checkout</li>
        </ol>
        <!-- Hero Content-->
        <div class="hero-content pb-5 text-center">
            <h1 class="hero-heading">Checkout</h1>
            <div class="row">
                <div class="col-xl-8 offset-xl-2"><p class="lead text-muted"><?php
                        if ($view == 'address') {
                            echo 'Please fill in your address.';
                        }elseif ($view == 'delivery') {
                            echo 'Shipping and additional costs are calculated based on values you have entered.';
                        }elseif ($view == 'payment') {
                            echo 'Choose the payment method.';
                        }elseif ($view == 'review') {
                            echo 'Please review your order.';
                        }
                        ?>
                    </p></div>
            </div>
        </div>
    </div>
</section>
<!-- Checkout-->
<section>
    <div class="container">
        <div class="row">

            <?php
            require 'views/checkout/'.$view.'.php';
            require 'views/checkout/layout/details.php';
            ?>
        </div>
    </div>
</section>
<?php
}
