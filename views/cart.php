<!-- Hero Section-->
<section class="hero">
    <div class="container">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active">Shopping cart</li>
        </ol>
        <!-- Hero Content-->
        <div class="hero-content pb-5 text-center">
            <h1 class="hero-heading">Shopping cart</h1>
            <div class="row">
                <div class="col-xl-8 offset-xl-2"><p class="lead text-muted">You have {{totalCount}} item(s) in your shopping cart</p></div>
            </div>
        </div>
    </div>
</section>
<!-- Shopping Cart Section-->
<section>
    <div class="container">
        <div class="row mb-5" ng-show="Shopcart.length > 0">
            <div class="col-lg-8">
                <div class="cart">
                    <div class="cart-wrapper">
                        <div class="cart-header text-center">
                            <div class="row">
                                <div class="col-5">Item</div>
                                <div class="col-2">Price</div>
                                <div class="col-2">Quantity</div>
                                <div class="col-2">Total</div>
                                <div class="col-1"></div>
                            </div>
                        </div>
                        <div class="cart-body" ng-repeat="item in Shopcart | orderBy:'id'">
                            <!-- Product-->
                            <div class="cart-item">
                                <div class="row d-flex align-items-center text-center">
                                    <div class="col-5">
                                        <div class="d-flex align-items-center">
                                            <a href="shop/product/{{item.slug}}">
                                                <img src="<?php echo config::baseUploadProductUrl(); ?>{{item.pic}}" alt="{{item.name}}" class="cart-item-img">
                                            </a>
                                            <div class="cart-title text-left">
                                                <a href="shop/product/{{item.slug}}" class="text-uppercase text-dark"><strong>{{item.name}}</strong></a>
                                                <br>
                                                <span class="text-muted text-sm">Size: {{item.size}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2">{{item.unit | currency:'₦':0}}</div>
                                    <div class="col-2">
                                        <div class="d-flex align-items-center">
                                            <div class="btn btn-items btn-items-decrease" ng-click="qtyUpdate(item.id, 'decrease')">-</div>
                                            <input type="text" value="{{item.qty}}" class="form-control text-center input-items">
                                            <div class="btn btn-items btn-items-increase" ng-click="qtyUpdate(item.id, 'increase')">+</div>
                                        </div>
                                    </div>
                                    <div class="col-2 text-center">{{item.cost | currency:'₦':0}}</div>
                                    <div class="col-1 text-center"><a href="javascript:;" class="cart-remove" ng-click="ejecter(item.id, 'false')"> <i class="fa fa-times"></i></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="my-5 d-flex justify-content-between flex-column flex-lg-row"><a href="shop" class="btn btn-link text-muted"><i class="fa fa-chevron-left"></i> Continue Shopping</a><a href="checkout" class="btn btn-dark">Proceed to checkout <i class="fa fa-chevron-right"></i>                                                     </a></div>
            </div>
            <div class="col-lg-4">
                <div class="block mb-5">
                    <div class="block-header">
                        <h6 class="text-uppercase mb-0">Order Summary</h6>
                    </div>
                    <div class="block-body bg-light pt-1">
                        <p class="text-sm">Shipping and additional costs are calculated based on values you have entered.</p>
                        <ul class="order-summary mb-0 list-unstyled">
                            <li class="order-summary-item"><span>Order Subtotal </span><span>{{cartTotal | currency:'₦':0}}</span></li>
                            <li class="order-summary-item"><span>Shipping and handling</span><span>₦0.00</span></li>
                            <li class="order-summary-item"><span>Tax</span><span>₦0.00</span></li>
                            <li class="order-summary-item border-0"><span>Total</span><strong class="order-summary-total">{{cartTotal | currency:'₦':0}}</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-5" ng-show="Shopcart.length === 0">
            <div class="col-md-12">
                <div class="alert alert-info text-center">No product has been added to your cart</div>
            </div>
        </div>
    </div>
</section>