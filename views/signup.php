<div class="container">
    <h1 class="bold text-center">Sign Up</h1>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-12 col-md-6">
            <form action="" class="mb-5 bordered-form">
                <div class="signup mt-3">
                    <div class="col-12  mb-2">
                        <label for="First Name">FIRST NAME</label> <br>
                        <input type="text" id="SignupNameInput1" placeholder="Didi" required>
                    </div>
                </div>
                <div class="signup mt-3">
                    <div class="col-12  mb-2">
                        <label for="Last Name">LAST NAME</label> <br>
                        <input type="text" id="SignupNameInput1" placeholder="" required>
                    </div>
                </div>
                <div class="signup mt-3 mb-2">
                    <div class="col-12 mb-2">
                        <label for="Email">EMAIL</label> <br>
                        <input type="text" id="SignupEmailInput" placeholder="janelle@clothing.com" required>
                    </div>
                </div>
                <div class="signup mt-3 mb-2">
                    <div class="signup mt-3 mb-2">
                        <div class="col-12 mb-2">
                            <label for="First Name">MOBILE NUMBER</label> <br>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon001">+234</span>
                                </div>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="signup mt-3 mb-2">
                    <div class="col-12 mb-2">
                        <label for="Email">PASSWORD</label> <br>
                        <input type="password" id="SignupPasswordInput" placeholder="Enter password" required>
                        <a href="#"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                    </div>
                </div>


                <div class="row">
                    <div class="signup col-12 col-md-12">
                        <div class="mt-3 mb-4 d-flex justify-content-center">
                            <button class="signup-btn">CREATE ACCOUNT</button>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class=" mt-2 text-center col-md-12">
                        <div class="mb-3 ridged-border-top"></div>
                        <p class=" mt-2 ">I already have an Account</p>
                    </div>
                </div>
                <div class="row">
                    <div class="signup col-12 col-md-12">
                        <div class="mt-3 mb-4 d-flex justify-content-center">

                            <button class="btn-login">LOGIN</button>
                        </div>
                    </div>
                </div>


            </form>
        </div>
        <div class="col-md-3"></div>



    </div>

</div>