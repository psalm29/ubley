
<footer class="main-footer">
    <!-- Main block - menus, subscribe form-->
    <div class="py-6 bg-gray-300 text-muted">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 mb-5 mb-lg-0">
                    <div class="font-weight-bold text-uppercase text-lg text-dark mb-3">
                        <img src="<?php echo config::logo(); ?>" alt="logo">
                    </div>
                    <p>We make fashion look simple until you step into the crowd and be the show stopper.</p>
                    <ul class="list-inline">
                        <li class="list-inline-item"><a href="#" target="_blank" title="facebook" class="text-muted text-hover-primary"><i class="fab fa-facebook"></i></a></li>
                        <li class="list-inline-item"><a href="#" target="_blank" title="instagram" class="text-muted text-hover-primary"><i class="fab fa-instagram"></i></a></li>
                        <li class="list-inline-item"><a href="#" target="_blank" title="twitter" class="text-muted text-hover-primary"><i class="fab fa-twitter"></i></a></li>
                        <li class="list-inline-item"><a href="#" target="_blank" title="pinterest" class="text-muted text-hover-primary"><i class="fab fa-pinterest"></i></a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6 mb-5 mb-lg-0">
                    <h6 class="text-uppercase text-dark mb-3">Shop</h6>
                    <?php
                    $categories = category::all();
                    if ($categories) {
                        ?>
                        <ul class="list-unstyled">
                            <?php
                            foreach ($categories as $category) {
                                ?>
                                <li> <a href="shop/category/<?php echo $category['slug']; ?>" class="text-muted"><?php echo $category['name']; ?></a></li>
                                <?php
                            }
                            ?>
                        </ul>
                        <?php
                    }
                    ?>

                </div>
                <div class="col-lg-2 col-md-6 mb-5 mb-lg-0">
                    <h6 class="text-uppercase text-dark mb-3">Company</h6>
                    <ul class="list-unstyled">
                        <?php
                        if (!isset($_SESSION['logged_user'])) {
                            ?>
                        <li> <a href="account" class="text-muted">Login</a></li>
                        <li> <a href="account" class="text-muted">Register</a></li>
                        <?php
                        }
                        ?>
                        <li> <a href="about" class="text-muted">About Us</a></li>
                        <li> <a href="contact" class="text-muted">Contacts</a></li>
                        <li> <a href="terms" class="text-muted">Terms and Conditions</a></li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <h6 class="text-uppercase text-dark mb-3">Daily Offers & Discounts</h6>
                    <p class="mb-3">Join our newsletter.</p>
                    <form action="" method="post" id="newsletter-form">
                        <div class="input-group mb-3">
                            <input type="email" name="email" required placeholder="Your Email Address" aria-label="Your Email Address" class="form-control bg-transparent border-secondary border-right-0">
                            <div class="input-group-append">
                                <button type="submit" name="newsletter" class="btn btn-outline-secondary border-left-0"> <i class="fa fa-paper-plane text-lg text-dark"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Copyright section of the footer-->
    <div class="py-4 font-weight-light bg-gray-800 text-gray-300">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 text-center text-md-left">
                    <p class="mb-md-0">&copy; <?php echo date('Y'); ?> Developed by <a href="https://codekago.com" target="_blank" style="color: #fff; ">Codekago Interactive</a>.  All rights reserved.</p>
                </div>
                <div class="col-md-6">
                    <ul class="list-inline mb-0 mt-2 mt-md-0 text-center text-md-right">
                        <li class="list-inline-item"><img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/img/visa.5210170f.svg" alt="..." class="w-2rem"></li>
                        <li class="list-inline-item"><img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/img/mastercard.ea104053.svg" alt="..." class="w-2rem"></li>
                        <li class="list-inline-item"><img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/img/paypal.abed015f.svg" alt="..." class="w-2rem"></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- /Footer end-->
<div id="scrollTop"><i class="fa fa-long-arrow-alt-up"></i></div>

<!-- JavaScript files-->
<script>
    // ------------------------------------------------------- //
    //   Inject SVG Sprite -
    //   see more here
    //   https://css-tricks.com/ajaxing-svg-sprite/
    // ------------------------------------------------------ //
    function injectSvgSprite(path) {

        var ajax = new XMLHttpRequest();
        ajax.open("GET", path, true);
        ajax.send();
        ajax.onload = function(e) {
            var div = document.createElement("div");
            div.className = 'd-none';
            div.innerHTML = ajax.responseText;
            document.body.insertBefore(div, document.body.childNodes[0]);
        }
    }
    // this is set to Bootstrapious website as you cannot
    // inject local SVG sprite (using only 'icons/orion-svg-sprite.a4dea202.svg' path)
    // while using file:// protocol
    // pls don't forget to change to your domain :)
    injectSvgSprite('https://demo.bootstrapious.com/sell/1-2-0/icons/orion-svg-sprite.svg');

</script>
<!-- jQuery-->
<script src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap JavaScript Bundle (Popper.js included)-->
<script src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Owl Carousel-->
<script src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/vendor/owl.carousel/owl.carousel.js"></script>
<script src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.min.js"></script>
<!-- NoUI Slider (price slider)-->
<script src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/vendor/nouislider/nouislider.min.js"></script>
<!-- Smooth scrolling-->
<script src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/vendor/smooth-scroll/smooth-scroll.polyfills.min.js"></script>
<!-- Lightbox -->
<script src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/vendor/ekko-lightbox/ekko-lightbox.min.js"></script>
<!-- Object Fit Images - Fallback for browsers that don't support object-fit-->
<script src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/vendor/object-fit-images/ofi.min.js"></script>
<script>
    var basePath = ''

</script>
<script src="assets/js/theme.1cfe87dd.js"></script>
<script src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/vendor/jquery-parallax.js/parallax.min.js"></script>
<script src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/vendor/jquery.cookie/jquery.cookie.js"> </script>
<script src="assets/js/demo.36f8799a.js"></script>

<script src="assets/js/lightbox.min.js"></script>


<script type="text/javascript" src="assets/js/angular.min.js"></script>
<script type="text/javascript" src="assets/js/app.js"></script>

</body>
</html>