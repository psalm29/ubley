<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo config::name().' | '.ucfirst($page); ?></title>
    <base href="<?php echo config::base(); ?>">
    <?php echo config::meta(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Price Slider Stylesheets -->
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/vendor/nouislider/nouislider.css">
    <!-- Google fonts - Playfair Display-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700">
    <link rel="stylesheet" href="assets/fonts/hkgrotesk/stylesheet.5c4e45c1.css">
    <!-- owl carousel-->
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/vendor/owl.carousel/assets/owl.carousel.css">
    <!-- Ekko Lightbox-->
    <link rel="stylesheet" href="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/vendor/ekko-lightbox/ekko-lightbox.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="assets/css/style.default.ac2b889b.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="assets/css/custom.0a822280.css">

    <link rel="stylesheet" href="assets/css/lightbox.min.css">

    <!-- Favicon-->
    <link rel="shortcut icon" href="<?php echo config::favicon(); ?>" />
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/solid.css" integrity="sha384-TbilV5Lbhlwdyc4RuIV/JhD8NR+BfMrvz4BL5QFa2we1hQu6wvREr3v6XSRfCTRp" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/regular.css" integrity="sha384-avJt9MoJH2rB4PKRsJRHZv7yiFZn8LrnXuzvmZoD3fh1aL6aM6s0BBcnCvBe6XSD" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/brands.css" integrity="sha384-7xAnn7Zm3QC1jFjVc1A6v/toepoG3JXboQYzbM0jrPzou9OFXm/fY6Z/XiIebl/k" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/fontawesome.css" integrity="sha384-ozJwkrqb90Oa3ZNb+yKFW2lToAWYdTiF1vt8JiH5ptTGHTGcN7qdoR1F95e0kYyG" crossorigin="anonymous">
</head>
<body ng-controller="janelleclothings">
<header class="header <?php
if ($page == 'home') {
//    echo 'header-absolute';
}
?>">

    <!-- Navbar-->
    <nav class="navbar navbar-expand-lg navbar-sticky navbar-airy navbar-light bg-white bg-fixed-white">
        <div class="container-fluid">
            <!-- Navbar Header  --><a href="/" class="navbar-brand">
                <img src="<?php echo config::logo(); ?>" alt="logo">
            </a>
            <button type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler navbar-toggler-right"><i class="fa fa-bars"></i></button>
            <!-- Navbar Collapse -->
            <div id="navbarCollapse" class="collapse navbar-collapse">
                <ul class="navbar-nav mx-auto">
                    <li class="nav-item dropdown"><a id="homeDropdownMenuLink" href="/" class="nav-link <?php
                    if ($page == 'home') {
                        echo 'active';
                    }
                    ?>">
                            Home
                        </a>
                    </li>
                    <li class="nav-item"><a href="shop" class="nav-link <?php
                        if ($page == 'shop') {
                            echo 'active';
                        }
                        ?>">Shop</a>
                    </li>
                    <?php
                    $categories = category::all();
                    if ($categories) {
                        ?>
                        <li class="nav-item dropdown"><a id="navbarDropdownMenuLink" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">Categories<i class="fa fa-angle-down"></i></a>
                            <ul aria-labelledby="navbarDropdownMenuLink" class="dropdown-menu">
                                <?php
                                foreach ($categories as $category) {
                                    ?>
                                <li><a href="shop/category/<?php echo $category['slug']; ?>" class="dropdown-item"><?php echo $category['name']; ?></a></li>
                                <?php
                                }
                                ?>
                            </ul>
                        </li>
                    <?php
                    }
                    ?>

                    <li class="nav-item"><a href="lookbook" class="nav-link <?php
                        if ($page == 'lookbook') {
                            echo 'active';
                        }
                        ?>">Lookbook</a>
                    </li>
                    <li class="nav-item"><a href="about" class="nav-link <?php
                        if ($page == 'about') {
                            echo 'active';
                        }
                        ?>">About Us</a>
                    </li>
                    <li class="nav-item"><a href="contact" class="nav-link <?php
                        if ($page == 'contact') {
                            echo 'active';
                        }
                        ?>">Contact</a>
                    </li>
                </ul>
                <div class="d-flex align-items-center justify-content-between justify-content-lg-end mt-1 mb-2 my-lg-0">
                    <!-- Search Button-->
                    <div data-toggle="search" class="nav-item navbar-icon-link">
                        <svg class="svg-icon">
                            <use xlink:href="#search-1"> </use>
                        </svg>
                    </div>
                    <!-- User Not Logged - link to login page-->

                        <?php

                        if (isset($_SESSION['logged_user'])) {
                            ?>
                            <div class="nav-item dropdown">
                                <a id="userdetails" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="dropdown-toggle navbar-icon-link">
                                    <svg class="svg-icon">
                                        <use xlink:href="#male-user-1"> </use>
                                    </svg>
                                </a>
                                <div aria-labelledby="userdetails" class="dropdown-menu dropdown-menu-right">
                                    <a href="account/orders" class="dropdown-item">Orders</a>
                                    <a href="account/address" class="dropdown-item">Address</a>
                                    <a href="account/profile" class="dropdown-item">Profile</a>
                                    <div class="dropdown-divider my-0"></div>
                                    <a href="account/logout" class="dropdown-item">Logout</a>
                                </div>
                            </div>
                        <?php
                        }else {
                            ?>
                            <div class="nav-item">
                                <a href="account/login" class="navbar-icon-link">
                                    <svg class="svg-icon">
                                        <use xlink:href="#male-user-1"> </use>
                                    </svg><span class="text-sm ml-2 ml-lg-0 text-uppercase text-sm font-weight-bold d-none d-sm-inline d-lg-none">Log in</span>
                                </a>
                            </div>
                        <?php
                        }

                        ?>

                    <!-- Cart Dropdown-->
                    <div class="nav-item dropdown"><a href="cart" class="navbar-icon-link d-lg-none">
                            <svg class="svg-icon">
                                <use xlink:href="#cart-1"> </use>
                            </svg><span class="text-sm ml-2 ml-lg-0 text-uppercase text-sm font-weight-bold d-none d-sm-inline d-lg-none">View cart</span></a>
                        <div class="cartMenu d-none d-lg-block"><a id="cartdetails" href="javascript:;" data-target="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="navbar-icon-link dropdown-toggle">
                                <svg class="svg-icon">
                                    <use xlink:href="#cart-1"> </use>
                                </svg>
                                <div class="navbar-icon-link-badge">{{totalCount}}</div></a>
                            <div aria-labelledby="cartdetails" class="cartMenu dropdown-menu dropdown-menu-right p-4">
                                <div class="navbar-cart-product-wrapper" ng-repeat="item in Shopcart | orderBy:'id'">
                                    <!-- cart item-->
                                    <div class="navbar-cart-product">
                                        <div class="d-flex align-items-center">
                                            <a href="shop/product/{{item.slug}}"><img src="<?php echo config::baseUploadProductUrl(); ?>{{item.pic}}" alt="{{item.name}}" class="img-fluid navbar-cart-product-image"></a>
                                            <div class="w-100"><a href="javascript:;" class="close text-sm mr-2" ng-click="ejecter(item.id, 'true')"><i class="fa fa-times">                                                   </i></a>
                                                <div class="pl-3"> <a href="shop/product/{{item.slug}}" class="navbar-cart-product-link">{{item.name}}</a>
                                                    <small class="d-block text-muted">Quantity: {{item.qty}} <span class="text-dark">|</span> Size: {{item.size}}</small>
                                                    <strong class="d-block text-sm">{{item.cost | currency:'₦':0}}</strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="alert alert-info text-center" ng-show="Shopcart.length === 0" style="min-width: 297px;">No product has been added to your cart</div>
                                <!-- total price-->
                                <div class="navbar-cart-total"><span class="text-uppercase text-muted">Total</span><strong class="cart-totale text-uppercase">{{cartTotal | currency:'₦':0}}</strong></div>
                                <!-- buttons-->
                                <div class="d-flex justify-content-between" ng-hide="Shopcart.length === 0">
                                    <a href="cart" class="btn btn-link text-dark mr-3">View Cart <i class="fa-arrow-right fa"></i></a><a href="checkout" class="btn btn-outline-dark">Checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- /Navbar -->

    <!-- Fullscreen search area-->
    <div class="search-area-wrapper">
        <div class="search-area d-flex align-items-center justify-content-center">
            <div class="close-btn">
                <svg class="svg-icon svg-icon-light w-3rem h-3rem">
                    <use xlink:href="#close-1"> </use>
                </svg>
            </div>
            <form action="shop/search" method="post" class="search-area-form">
                <div class="form-group position-relative">
                    <input type="search" name="search" id="search" placeholder="What product are you looking for?" class="search-area-input">
                    <button type="submit" class="search-area-button">
                        <svg class="svg-icon">
                            <use xlink:href="#search-1"> </use>
                        </svg>
                    </button>
                </div>
            </form>
        </div>
    </div>
    <!-- /Fullscreen search area-->

</header>

<div id="toast" style="padding: 7px 15px; margin-bottom: 20px !important; max-width: 300px; display: none;">
    <button type="button" class="close" onclick="$('#toast').css('display', 'none');" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <center>
        <p id="toastMsg" style="margin-bottom: 0px; line-height: normal;"></p>
    </center>
</div>

<?php

if (isset($_POST['newsletter'])) {
    ?>
<div style="margin-bottom: -20px;">
    <?php
        newsletter::add($_POST['email']);
    ?>
</div>
<?php
}