<div class="container">
    <h1 class="bold text-center">Log In</h1>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-12 col-md-6">
            <form action="" class="mb-5 bordered-form">
                <div class="login mt-3 mb-2">
                    <div class="col-12 mb-2">
                        <label for="Email">EMAIL</label> <br>
                        <input type="text" id="LoginEmailInput" placeholder="janelle@clothing.com" required>
                    </div>
                </div>

                <div class="login mt-3 mb-2">
                    <div class="col-12 mb-2">
                        <div class="d-flex justify-content-between">

                            <label for="Password">PASSWORD</label>
                            <span class=""> &nbsp;&nbsp;<a href="#" class="forgot ml-5 mb-3"> Forgot Password</a></span> <br>
                        </div>
                        <input type="password" id="LoginPasswordInput" placeholder="Enter password" required>
                        <a href="#"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                    </div>
                </div>


                <div class="row">
                    <div class="login col-12 col-md-12">
                        <div class="mt-3 mb-4 d-flex justify-content-center">
                            <button class="signup-btn">LOGIN</button>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class=" mt-2 text-center col-md-12">
                        <div class="mb-3 ridged-border-top"></div>
                        <p class=" mt-2 ">I don't have an Account</p>
                    </div>
                </div>
                <div class="row">
                    <div class="login col-12 col-md-12">
                        <div class="mt-3 mb-4 d-flex justify-content-center">

                            <button class="btn-login">SIGNUP</button>
                        </div>
                    </div>
                </div>


            </form>
        </div>
        <div class="col-md-3"></div>



    </div>

</div>