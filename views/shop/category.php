<?php

if ($routes[3] != '' AND $routes[3] != 'shop') {
    $slug = $routes[3];
    $category = category::check($slug);

    if (!$category) {
        header('location: ../shop');
    }else {
        $id = $category['id'];
    }

}else {
    header('location: ../shop');
}

?>

<section class="hero">
    <div class="container">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item"><a href="shop">Shop</a></li>
            <li class="breadcrumb-item active"><?php echo $category['name']; ?></li>
        </ol>
        <!-- Hero Content-->
        <div class="hero-content pb-5 text-center">
            <h1 class="hero-heading"><?php echo $category['name']; ?></h1>
        </div>
    </div>
</section>
<div class="container-fluid">
    <div class="px-xl-5">
        <div class="row">
            <?php

            require 'views/shop/layout/sidemenu.php';

            ?>
            <!-- Grid -->
            <div class="products-grid col-xl-9 col-lg-8 order-lg-2">
                <header class="product-grid-header">
                    <div class="mr-3 mb-3">

                        Showing <strong>1-12 </strong>of <strong>158 </strong>products
                    </div>
                    <div class="mr-3 mb-3"><span class="mr-2">Show</span><a href="#" class="product-grid-header-show active">12    </a><a href="#" class="product-grid-header-show ">24    </a><a href="#" class="product-grid-header-show ">All    </a>
                    </div>
                </header>
                <div class="row">
                    <?php

                    $products = category::products($id);

                    if ($products) {

                        foreach ($products as $product) {
                            product::display($product);
                        }

                    }else {
                        respond::alert('info', '', 'No product has been added to this category');
                    }

                    ?>
                </div>
<!--                <nav aria-label="page navigation" class="d-flex justify-content-center mb-5 mt-3">-->
<!--                    <ul class="pagination">-->
<!--                        <li class="page-item"><a href="#" aria-label="Previous" class="page-link"><span aria-hidden="true">Prev</span><span class="sr-only">Previous</span></a></li>-->
<!--                        <li class="page-item active"><a href="#" class="page-link">1       </a></li>-->
<!--                        <li class="page-item"><a href="#" class="page-link">2       </a></li>-->
<!--                        <li class="page-item"><a href="#" class="page-link">3       </a></li>-->
<!--                        <li class="page-item"><a href="#" class="page-link">4       </a></li>-->
<!--                        <li class="page-item"><a href="#" class="page-link">5 </a></li>-->
<!--                        <li class="page-item"><a href="#" aria-label="Next" class="page-link"><span aria-hidden="true">Next</span><span class="sr-only">Next     </span></a></li>-->
<!--                    </ul>-->
<!--                </nav>-->
            </div>
            <!-- / Grid End-->

        </div>
    </div>
</div>
