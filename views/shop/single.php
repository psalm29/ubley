<?php

if ($routes[3] != '' AND $routes[3] != 'shop') {
    $slug = $routes[3];
    $product = product::single($slug);

    if (!$product) {
        header('location: ../shop');
    }else {
        $id = $product['id'];
    }

}else {
    header('location: ../shop');
}

?>
<section class="product-details">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 py-3 order-2 order-lg-1">
                <div data-slider-id="1" class="owl-carousel owl-theme owl-dots-modern detail-full">
                    <div style="background: center center url(<?php echo config::baseUploadProductUrl().$product['thumbnail']; ?>) no-repeat; background-size: cover;" class="detail-full-item"></div>
                    <?php

                    $galleries = gallery::all($id);

                    if ($galleries) {

                        foreach ($galleries as $gallery) {
                            ?>
                            <div style="background: center center url(<?php echo config::baseUploadProductGalleryUrl().$gallery['image']; ?>) no-repeat; background-size: cover;" class="detail-full-item"></div>
                            <?php
                        }

                    }

                    ?>

                </div>
            </div>
            <div class="d-flex align-items-center col-lg-6 col-xl-5 pl-lg-5 mb-5 order-1 order-lg-2">
                <div>
                    <ul class="breadcrumb justify-content-start">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item"><a href="shop">Shop</a></li>
                        <li class="breadcrumb-item active">Product Detail</li>
                    </ul>
                    <h1 class="mb-4"><?php echo $product['name']; ?></h1>
                    <div class="d-flex flex-column flex-sm-row align-items-sm-center justify-content-sm-between mb-4">
                        <ul class="list-inline mb-2 mb-sm-0">
                            <li class="list-inline-item h4 font-weight-light mb-0">₦<?php echo number_format($product['price']); ?></li>
                            <?php
                            if ($product['cost_price'] != 0) {
                                ?>
                            <li class="list-inline-item text-muted font-weight-light">
                                <del>₦<?php echo number_format($product['cost_price']); ?></del>
                            </li>
                            <?php
                            }
                            ?>

                        </ul>
                        <?php
                        if ($product['discount'] != 0) {
                            ?>
                        <div class="d-flex align-items-center">
                            <span class="text-dark text-uppercase text-sm"><?php echo $product['discount'].'% discount'; ?></span>
                        </div>
                        <?php
                        }
                        ?>

                    </div>
                    <p class="mb-4 text-muted"><?php echo $product['description']; ?></p>

                        <div class="row">
                            <div class="col-sm-12 col-lg-12 detail-option mb-3">
                                <h6 class="detail-option-heading">Size <span>(required)</span></h6>
                                <label for="size_0" class="btn btn-sm btn-outline-secondary detail-option-btn-label">

                                    Small
                                    <input type="radio" name="size" ng-click="setSize('Small')" value="value_0" id="size_0" required class="input-invisible">
                                </label>
                                <label for="size_1" class="btn btn-sm btn-outline-secondary detail-option-btn-label">

                                    Medium
                                    <input type="radio" name="size" ng-click="setSize('Medium')" value="value_1" id="size_1" required class="input-invisible">
                                </label>
                                <label for="size_2" class="btn btn-sm btn-outline-secondary detail-option-btn-label">

                                    Large
                                    <input type="radio" name="size" ng-click="setSize('Large')" value="value_2" id="size_2" required class="input-invisible">
                                </label>
                            </div>
                            <div class="col-12 col-lg-6 detail-option mb-5" ng-controller="quat">
                                <label class="detail-option-heading font-weight-bold">Items <span>(required)</span></label>
                                <input name="items" type="number" ng-model="qty" min="1" ng-change="quantityChange(qty)" value="1" class="form-control detail-quantity">
                            </div>
                        </div>
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <button type="button" class="btn btn-dark btn-lg mb-1" ng-click="addCart('<?php echo $product['id'];?>','<?php echo $product['name'];?>','<?php echo $product['price'];?>','<?php echo $product['thumbnail'];?>', '<?php echo $product['slug'];?>')"> <i class="fa fa-shopping-cart mr-2"></i>Add to Cart</button>
                            </li>
                        </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="my-5">
    <div class="container">
        <header class="text-center">
            <h5 class="text-uppercase mb-5">You might also like</h5>
        </header>
        <div class="row">
            <?php

            $others = product::other_products($slug);

            if ($others) {

                foreach ($others as $other) {
                    product::display($other);
                }

            }else {
                respond::alert('info', '', 'No other product to display');
            }

            ?>
        </div>
    </div>
</section>