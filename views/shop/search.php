<?php

if (isset($_POST['search'])) {
    $product = request::secureTxt($_POST['search']);
    echo $product;
}else {
    header('location: ../shop');
}

?>
<!-- Hero Section-->
<section class="hero">
    <div class="container">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active">Search</li>
        </ol>
        <!-- Hero Content-->
        <div class="hero-content pb-5 text-center">
            <h1 class="hero-heading">Product Search</h1>
            <div class="row">
                <div class="col-xl-8 offset-xl-2"><p class="lead text-muted">Search results for <?php echo $product; ?></p></div>
            </div>
        </div>
    </div>
</section>
<main>
    <div class="container">
        <div class="row">
            <!-- Grid -->
            <div class="products-grid col-12 sidebar-none">
                <div class="row mb-5">
                    <?php

                    $products = product::search($product);

                    if ($products) {

                        foreach ($products as $product) {
                            product::display($product);
                        }

                    }else {
                        respond::alert('info', '', 'No search result for '.$product);
                    }

                    ?>
                </div>
            </div>
            <!-- / Grid End-->
        </div>
    </div>
</main>
