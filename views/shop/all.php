<section class="hero">
    <div class="container">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active">Shop</li>
        </ol>
        <!-- Hero Content-->
        <div class="hero-content pb-5 text-center">
            <h1 class="hero-heading">Shop</h1>
<!--            <div class="row">-->
<!--                <div class="col-xl-8 offset-xl-2"><p class="lead text-muted">Shop from our wide range of products.</p></div>-->
<!--            </div>-->
        </div>
    </div>
</section>
<div class="container-fluid">
    <div class="px-xl-5">
        <div class="row">
            <?php

            require 'views/shop/layout/sidemenu.php';

            ?>
            <!-- Grid -->
            <div class="products-grid col-xl-9 col-lg-8 order-lg-2">
                <header class="product-grid-header">
                    <div class="mr-3 mb-3">

                        Showing <strong>1-12 </strong>of <strong>158 </strong>products
                    </div>
                    <div class="mr-3 mb-3"><span class="mr-2">Show</span><a href="#" class="product-grid-header-show active">12    </a><a href="#" class="product-grid-header-show ">24    </a><a href="#" class="product-grid-header-show ">All    </a>
                    </div>
                </header>
                <div class="row">
                    <?php

                    $products = product::view();

                    if ($products) {

                        foreach ($products as $product) {
                            product::display($product);
                        }

                    }else {
                        respond::alert('info', '', 'No product available for purchase');
                    }

                    ?>
                </div>

            </div>
            <!-- / Grid End-->

        </div>
    </div>
</div>
