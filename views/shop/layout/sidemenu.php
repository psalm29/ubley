<!-- Sidebar-->
<div class="sidebar col-xl-3 col-lg-4 pr-xl-5 order-lg-1">
    <div class="sidebar-block px-3 px-lg-0">
        <h5 class="text-uppercase mb-4">Product Categories</h5>
        <div class="nav nav-pills flex-column mt-4 mt-lg-0">
            <?php

            $categories = category::all();

            if ($categories) {
                foreach ($categories as $category) {
                    $count = $db->query("SELECT count(*) FROM product_category WHERE category_id = :id", array('id' => $category['id']));
                    ?>
                    <a href="shop/category/<?php echo $category['slug']; ?>" class="nav-link d-flex <?php
                     if (isset($slug)) {
                         if ($slug == $category['slug']) {
                             echo 'active';
                         }
                     }
                     ?> justify-content-between mb-2 "><span><?php echo $category['name']; ?></span><span class="sidebar-badge"> <?php echo $count[0]['count(*)']; ?></span></a>
                    <?php
                }
            }

            ?>

        </div>
    </div>

</div>
<!-- /Sidebar end-->