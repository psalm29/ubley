<section>
    <div class="container-fluid">
        <div class="row my-lg-3">
            <div class="col-lg-6 order-lg-2 mb-3 mb-lg-0">
                <div class="d-flex align-items-center p-5 p-xl-6 bg-gray-100 h-100">
                    <div>
                        <h6 class="text-uppercase text-primary letter-spacing-5 mb-3"> About Us</h6>
                        <h1 class="display-3 text-serif font-weight-bold mb-5"><?php echo config::name(); ?></h1>
                        <div class="row">
                            <div class="col-xl-6">
                                <p class="text-lg text-muted mb-lg-0">One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like ba</p>
                            </div>
                            <div class="col-xl-6">
                                <p class="text-lg text-muted mb-0">The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of </p>
                            </div>
                        </div>
                        <hr class="my-5">
                        <div class="row">
                            <div class="col-xl-6">
                                <h6 class="text-uppercase mb-4">Store Location</h6>
                                <p class="text-lg text-muted mb-4 mb-xl-0"><?php echo config::address(); ?></p>
                            </div>
                            <div class="col-xl-6">
                                <h6 class="text-uppercase mb-4">Get in touch</h6>
                                <p class="text-lg text-muted mb-0"><?php echo config::phone(); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 pr-lg-0 order-lg-1">
                <div class="dark-overlay mh-full-screen-with-header h-100"><img src="assets/images/about.jpg" alt="" class="bg-image"></div>
            </div>
        </div>
    </div>
</section>