
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
    <!-- BEGIN PAGE HEAD -->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Lookbook</h1>
            </div>
            <!-- END PAGE TITLE -->
            <div class="page-title pull-right">
                <button class="btn btn-primary" data-target="#addModal" data-toggle="modal">New Lookbook</button>
            </div>

        </div>
    </div>
    <!-- END PAGE HEAD -->
    <!-- BEGIN PAGE CONTENT -->
    <div class="page-content">
        <div class="container">
            <!-- BEGIN PAGE CONTENT INNER -->
            <div class="row margin-top-10">
                <div class="col-md-12 col-xs-12">
                    <?php

                        if(isset($_POST['add'])){
                            lookbook::add($_POST['title'], $_POST['description'], $_FILES['file']);
                        }
                        // add lookbook

                        if(isset($_POST['edit'])){
                            lookbook::edit($_POST['id'], $_POST['title'], $_POST['description'], $_FILES['file']);
                        }
                        // Edit lookbook

                        if (isset($_POST['remove'])) {
                            lookbook::remove($_POST['id']);
                        }// Remove lookbook

                        if (isset($_POST['addGallery'])) {

                            if (!isset($_FILES['files'])) {
                                respond::alert('warning', '', 'An image must be selected to add to lookbook');
                            }else {
                                lookbook::addImages($_POST['id'], $_FILES['files']);
                            }

                        }// Add image to lookbook

                        if (isset($_POST['removeImage'])) {
                            lookbook::removeImages($_POST['id']);
                        }// Remove image from lookbook

                    ?>
                </div>
                <div class="col-md-12 col-sm-12">
                    <!-- BEGIN PORTLET-->
                    <div style="min-height:670px" class="portlet light ">
                        <div class="portlet-title">
                            <div class="caption caption-md">
                                <i class="icon-bar-chart theme-font hide"></i>
                            </div>
                            <div class="actions">
                                <div style="height:auto; text-align:center;background:#d6e9c6;padding:5px; width:auto;display:none" class="notif"></div>

                            </div>
                        </div>
                        <div class="portlet-body">
                            <div data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">

                                <div class="row">
                                    <div class="col-md-12 blog-page">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 article-block">
                                                <h1 style="margin-top:0px"></h1>
                                                <?php

                                                $lookbook = lookbook::all();

                                                if ($lookbook) {

                                                    foreach ($lookbook as $look) {
                                                        $id = $look['id'];
                                                        $title = $look['title'];
                                                        $description = $look['description'];
                                                        ?>
                                                        <div class="row">
                                                            <div class="col-md-3 blog-img blog-tag-data">
                                                                <img src="<?php echo config::baseUploadLookbookCoverUrl().$look['cover']; ?>" alt="" style="max-height: 200px;" class="img-responsive">
                                                                <ul class="list-inline">
                                                                    <li>
                                                                        <i class="fa fa-calendar"></i>
                                                                        <span>
														<?php
                                                        echo date("F d, Y", $look['timestamp']);
                                                        ?>
													</span>
                                                                    </li>

                                                                </ul>

                                                            </div>
                                                            <div class="col-md-9 blog-article">
                                                                <h4 class="media-heading">
                                                                    <?php echo $look['title']; ?>
                                                                </h4>
                                                                <p>
                                                                    <?php
                                                                    echo substr($description,0,100);
                                                                    ?>
                                                                <div class="clearfix"></div>
                                                                </p>
                                                                <div class="row">
                                                                    <?php

                                                                    $images = lookbook::allImages($id);

                                                                    if ($images) {

                                                                        foreach ($images as $image) {
                                                                            $image_id = $image['id'];
                                                                            ?>
                                                                            <div class="col-xs-4 col-md-2" style="margin-bottom: 20px;">
                                                                                <a href="<?php echo config::baseUploadLookbookUrl().$image['image']; ?>" data-lightbox="gallery<?php echo $id; ?>">
                                                                                    <img class="thumbnail" src="<?php echo config::baseUploadLookbookUrl().$image['image']; ?>" alt="<?php echo $look['title']; ?>" style="margin-bottom: 0px; height: 100px; width: 100px; object-fit: scale-down;">
                                                                                </a>
                                                                                <button class="btn red btn-xs" style="width: 100px;" data-toggle="modal" data-target="#removeGalleryModal" onclick="$('.image_id').val('<?php echo $image_id; ?>');">
                                                                                    Remove <i class="fa fa-trash"></i>
                                                                                </button>
                                                                            </div>
                                                                            <?php
                                                                        }

                                                                    }else {
                                                                        respond::alert('info', '', 'No image has been added to this lookbook');
                                                                    }

                                                                    ?>
                                                                </div>
                                                                <div class="btn-group">
                                                                    <button class="btn blue" data-toggle="modal" data-target="#addGalleryModal" onclick="$('.lookbook_id').val('<?php echo $id; ?>');">
                                                                        Add Image(s) <i class="fa fa-image"></i>
                                                                    </button>
                                                                    <button class="btn green" data-toggle="modal" data-target="#editModal" onclick="$('.lookbook_id').val('<?php echo $id; ?>'); $('.lookbook_title').val('<?php echo $title; ?>'); $('.lookbook_description').load('include/load_description.php', {'id': <?php echo $id; ?>, 'table': 'lookbook'});">
                                                                        Edit <i class="m-icon-swapright m-icon-white"></i>
                                                                    </button>
                                                                    <button class="btn red" data-toggle="modal" data-target="#removeModal" onclick="$('.lookbook_id').val('<?php echo $id; ?>');">
                                                                        Delete <i class="fa fa-trash"></i>
                                                                    </button>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <hr>
                                                        <?php
                                                    }

                                                }else {
                                                    respond::alert('info', '', 'No lookbook has been added');
                                                }

                                                ?>


                                            </div>
                                            <!--end col-md-9-->

                                        </div>






                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>

            </div>
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END PAGE CONTAINER -->



    <!-- Add Lookbook Modal -->
    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">New Lookbook</h4>
                    </div>
                    <div class="modal-body">


                        <div class="form-group">
                            <label class="control-label ">Title</label>
                            <input type="text" name="title" required placeholder="Lookbook Title" class="form-control"/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Cover Image</label>
                            <input type="file" required id="pic" class="form-control btn btn-primary" name="file">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Description</label>
                            <textarea name="description" id="text" placeholder="Description of lookbook" class="form-control" rows="6"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="add">Add Lookbook</button>
                        <button type="button" class="btn red" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Lookbook Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel"><span id="category_title"></span> Category</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" class="lookbook_id">
                        <div class="form-group">
                            <label class="control-label ">Title</label>
                            <input type="text" name="title" required placeholder="Lookbook Title" class="form-control lookbook_title"/>
                        </div>

                        <div class="form-group">
                            <label class="control-label">Cover Image</label>
                            <input type="file" id="pic" class="form-control btn btn-primary" name="file">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Description</label>
                            <textarea name="description" placeholder="Description of lookbook" class="form-control lookbook_description" rows="3"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="edit">Save Changes</button>
                        <button type="button" class="btn red" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Remove Lookbook Modal -->
    <div class="modal fade" id="removeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Remove Lookbook</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" class="lookbook_id">
                        <p align="center">Are you sure you want to remove this lookbook?</p>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="remove">Remove Lookbook</button>
                        <button type="button" class="btn red" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Remove Lookbook Image Modal -->
    <div class="modal fade" id="removeGalleryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Remove Image</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" class="image_id">
                        <p align="center">Are you sure you want to remove this image from lookbook?</p>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="removeImage">Remove Image</button>
                        <button type="button" class="btn red" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Lookbook Image Add Modal -->
    <div class="modal fade" id="addGalleryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Lookbook Image</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="id" class="lookbook_id">
                        <div class="form-group">
                            <label class="control-label">Lookbook Image(s)</label>
                            <input type="file" multiple class="form-control btn btn-primary" name="files[]" required>
                            <p>Multiple images can be selected</p>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" name="addGallery">Add Image(s)</button>
                        <button type="button" class="btn red" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>