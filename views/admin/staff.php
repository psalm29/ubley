
<!-- BEGIN PAGE CONTAINER -->
<div class="page-container">
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<div class="container">
			<!-- BEGIN PAGE TITLE -->
			<div class="page-title">
				<h1>Staff Accounts</h1>
			</div>
            <!-- END PAGE TITLE -->
            <div class="page-title pull-right">
                <button class="btn btn-primary" data-target="#addStaff" data-toggle="modal">Add Staff</button>
            </div>
		</div>
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE CONTENT -->
	<div class="page-content">
		<div class="container">

            <?php

            if (isset($_POST['editStaff'])) {
                staff::edit($_POST['staff_id'], $_POST['name'], $_POST['email'], $_POST['phone'], $_POST['username'], $_POST['address']);
            }

            if (isset($_POST['removeStaff'])) {
                staff::remove($_POST['staff_id']);
            }

            if (isset($_POST['addStaff'])) {
                staff::add($_POST['username'], $_POST['name'], $_POST['email'], $_POST['phone'], $_POST['address'], $_POST['password']);
            }

            ?>

			<div class="portlet light">
				<div class="portlet-body">

                    <?php

                    $staff_accounts = staff::all();

                    if ($staff_accounts) {

                        ?>
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Email Address</th>
                            <th>Phone Number</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach ($staff_accounts as $staff) {
                            $email = $staff['email'];
                            $staff_id = $staff['id'];
                            $name = $staff['name'];
                            $phone = $staff['phone'];
                            $username = $staff['username'];
                            $address = $staff['address'];
                            ?>
                        <tr>
                            <td><?php echo $staff['name']; ?></td>
                            <th><?php echo $staff['username']; ?></th>
                            <td><?php echo $staff['email']; ?></td>
                            <td><?php echo $staff['phone']; ?></td>
                            <td>
                                <button class="btn btn-primary btn-sm mr-2" onclick="$('.staff_id').val('<?php echo $staff_id; ?>'); $('#staff_email').val('<?php echo $email; ?>'); $('#staff_phone').val('<?php echo $phone; ?>'); $('#staff_name').val('<?php echo $name; ?>'); $('#address').val('<?php echo $address; ?>'); $('#username').val('<?php echo $username; ?>');" data-toggle="modal" data-target="#editStaff">Edit Account</button>
                                <button class="btn btn-danger btn-sm" data-target="#removeStaff" data-toggle="modal" onclick="$('.staff_id').val('<?php echo $staff_id; ?>');">Remove</button>
                            </td>
                        </tr>
                        <?php
                        }

                        ?>

                        </tbody>
                    </table>
                    <?php

                    }else {
                        respond::alert('info', '', 'No staff account has been created');
                    }

                    ?>

				</div>
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->

<!-- ADD STAFF ACCOUNT Modal -->
<div class="modal fade" id="addStaff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="post">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">New Staff</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="staff_id" class="staff_id">
                    <div class="form-group">
                        <label for="" class="mb-2">Name</label>
                        <input type="text" name="name" placeholder="Staff Name" class="form-control">
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="mb-2">Email Address</label>
                                <input type="email" name="email"  required placeholder="Email Address" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="mb-2">Phone Number</label>
                                <input type="text" name="phone" required placeholder="Phone Number" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="mb-2">Username</label>
                                <input type="text" required name="username" placeholder="Staff Username" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="mb-2">Password</label>
                                <input type="password" required name="password" placeholder="Staff Password" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="mb-2">Address</label>
                        <input type="text" name="address" placeholder="Staff Address" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="addStaff" class="btn btn-link">Add Staff</button>
                    <button type="button" class="btn btn-danger text-white" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- EDIT STAFF ACCOUNT Modal -->
<div class="modal fade" id="editStaff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="" method="post">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Edit Staff</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="staff_id" class="staff_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="mb-2">Name</label>
                                <input type="text" name="name" id="staff_name" placeholder="Staff Name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="mb-2">Username</label>
                                <input type="text" name="username" id="username" placeholder="Staff Address" class="form-control">
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="mb-2">Email Address</label>
                                <input type="email" name="email" id="staff_email" required placeholder="Email Address" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="" class="mb-2">Phone Number</label>
                                <input type="text" name="phone" id="staff_phone" required placeholder="Phone Number" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="mb-2">Address</label>
                        <input type="text" name="address" id="address" placeholder="Staff Address" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" name="editStaff" class="btn btn-primary">Save Changes</button>
                    <button type="button" class="btn btn-danger text-white" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Remove STAFF FROM Branch Modal -->
<div class="modal fade" id="removeStaff" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form action="" method="post">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Remove Staff</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="staff_id" class="staff_id">

                    <p class="text-center">
                        Are sure you want to remove this staff?
                    </p>

                </div>
                <div class="modal-footer">
                    <button type="submit" name="removeStaff" class="btn btn-link">Yes, Remove</button>
                    <button type="button" class="btn btn-danger text-white" data-dismiss="modal">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
