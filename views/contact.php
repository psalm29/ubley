<section class="hero">
    <div class="container">
        <!-- Breadcrumbs -->
        <ol class="breadcrumb justify-content-center">
            <li class="breadcrumb-item"><a href="/">Home</a></li>
            <li class="breadcrumb-item active">Contact</li>
        </ol>
        <!-- Hero Content-->
        <div class="hero-content pb-5 text-center">
            <h1 class="hero-heading">Contact</h1>

        </div>
    </div>
</section>
<section style="background: #fafafa;" class="py-6">
    <div class="container">
        <div class="row">
            <div class="col-md-4 text-center text-md-left">
                <svg class="svg-icon svg-icon-light text-primary w-3rem h-3rem mb-3">
                    <use xlink:href="#navigation-map-1"> </use>
                </svg>
                <h4 class="ff-base">Address</h4>
                <p class="text-muted"><?php echo config::address(); ?></p>
            </div>
            <div class="col-md-4 text-center text-md-left">
                <svg class="svg-icon svg-icon-light text-primary w-3rem h-3rem mb-3">
                    <use xlink:href="#audio-call-1"> </use>
                </svg>
                <h4 class="ff-base">Phone Numbers</h4>
<!--                <p class="text-muted">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>-->
                <p class="text-muted"><?php echo config::phone(); ?></p>
            </div>
            <div class="col-md-4 text-center text-md-left">
                <svg class="svg-icon svg-icon-light text-primary w-3rem h-3rem mb-3">
                    <use xlink:href="#mail-1"> </use>
                </svg>
                <h4 class="ff-base">Electronic support</h4>
                <p class="text-muted">Please feel free to write an email to us or to use our electronic ticketing system.</p>
                <ul class="list-unstyled text-muted">
                    <li><?php echo config::email(); ?></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="py-6">
    <div class="container">
        <header class="mb-5">
            <h2 class="text-uppercase h5">Contact form</h2>
        </header>
        <div class="row">
            <div class="col-md-7 mb-5 mb-md-0">
                <?php

                if (isset($_POST['contact'])) {

                    $from = $_POST['email'];
                    $sender = $_POST['name'];
                    $phone = $_POST['phone'];
                    $subject = "Contact Message";
                    $message = $_POST['message'];

                    $msg = '<p style="color: #3E4095; font-size: 28px;"><span><b style="color: #f58534;">New Contact Message</b></span></p>
                <p>
                <p style="color: #000000; font-size: 22px;">You have a new message from '.config::name().' website</p>
                </p><br>
                <p>
                    Name: '.$sender.'<br>
                    Email Address: '.$from.'<br>
                    Phone Number: '.$phone.'<br>
                    <span style="text-decoration: underline;">Message</span><br>
                    '.$message.'
                </p>';

                    mail::send($from, $sender, config::email(), $subject, $msg);

                    respond::alert('success', 'Message successfully sent!', 'We\'ll get back to you shortly');

                }

                ?>
                <form id="contact-form" method="post" action="" class="form">
                    <div class="controls">
                        <div class="form-group">
                            <label for="name" class="form-label">Your Name *</label>
                            <input type="text" name="name" id="name" placeholder="Enter your full name" required="required" class="form-control">
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="email" class="form-label">Your Email Address *</label>
                                    <input type="email" name="email" id="email" placeholder="Enter your email address" required="required" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="phone" class="form-label">Your Phone Number *</label>
                                    <input type="text" name="phone" id="phone" placeholder="Enter your phone number" required="required" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="message" class="form-label">Your message for us *</label>
                            <textarea rows="4" name="message" id="message" placeholder="Enter your message" required="required" class="form-control"></textarea>
                        </div>
                        <button type="submit" name="contact" class="btn btn-outline-dark">Send message</button>
                    </div>
                </form>
            </div>
            <div class="col-md-5">
                <h3 class="mb-3">Follow us on Social Media</h3>
                <div class="social">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="#" target="_blank"><i class="fab fa-facebook text-dark" style="font-size: 30px;"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="https://instagram.com/janelle_clothing" target="_blank"><i class="fab fa-instagram text-dark" style="font-size: 30px;"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" target="_blank"><i class="fab fa-twitter text-dark" style="font-size: 30px;"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="#" target="_blank"><i class="fab fa-pinterest text-dark" style="font-size: 30px;"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="map" style="height: 400px;"></div>