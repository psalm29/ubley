<section data-parallax="scroll" data-image-src="assets/images/lookbook.jpg" data-speed="0.3" data-position-x="right" class="position-relative dark-overlay py-3 py-lg-7 overflow-hidden mb-2">
    <div class="container overlay-content hero hero-page">
        <ul class="breadcrumb justify-content-center no-border mb-0">
            <li class="breadcrumb-item"><a href="/" class="text-white">Home</a></li>
            <li class="breadcrumb-item text-white active">Lookbook</li>
        </ul>
        <div class="hero-content pb-5 text-center text-white">
            <h1 class="hero-heading">Lookbook</h1><p class="lead">Take a look at some of our awesome fashion concepts</p>
        </div>
    </div>
</section>
<section>
    <div class="container-fluid px-5px">

        <div class="row mx-0">
            <div class="col-md-8">
                <div class="row">
                    <?php

                    $lookbook = lookbook::all();

                    if ($lookbook) {

                        foreach ($lookbook as $look) {
                            $id = $look['id'];
                            ?>
                            <div class="col-lg-6 mb-10px px-5px">
                                <div class="card border-0 text-center text-white">
                                    <img src="<?php echo config::baseUploadLookbookCoverUrl().$look['cover']; ?>" alt="<? echo $look['title']; ?>" class="card-img">
                                    <div class="card-img-overlay d-flex align-items-center">
                                        <div class="w-100">
                                            <h2 class="display-4 mb-4"><? echo $look['title']; ?></h2>
                                            <a href="<?php echo config::baseUploadLookbookCoverUrl().$look['cover']; ?>" data-title="<?php echo $look['title']; ?>" data-lightbox="gallery<?php echo $id; ?>" class="btn btn-light">View Lookbook</a>
                                            <?php

                                            $images = lookbook::allImages($look['id']);

                                            if ($images) {

                                                foreach ($images as $image) {
                                                    $image_id = $image['id'];
                                                    ?>
                                                    <a href="<?php echo config::baseUploadLookbookUrl().$image['image']; ?>" data-title="<?php echo $look['title']; ?>" data-lightbox="gallery<?php echo $id; ?>">
                                                    </a>
                                                    <?php
                                                }

                                            }

                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }

                    }else {
                        respond::alert('info', '', 'No lookbook has been added');
                    }

                    ?>
                </div>
            </div>
            <div class="col-md-4">
                <h2 class="mb-5">Follow us on Instagram</h2>
                <div class="card-columns product-columns">
                    <!-- product masonry-->
                    <div class="product card border-0">
                        <div class="product-image">
                            <img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/img/product/serrah-galos-494312-unsplash-cropped.89041235.jpg" alt="White Tee" class="img-fluid">
                        </div>
                    </div>

                    <div class="product card border-0">
                        <div class="product-image">
                            <img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/img/product/kyle-loftus-596319-unsplash.5ef14d25.jpg" alt="White Tee" class="img-fluid">
                        </div>
                    </div>

                    <div class="product card border-0">
                        <div class="product-image">
                            <img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/img/product/ethan-haddox-484912-unsplash-cropped.499d5447.jpg" alt="White Tee" class="img-fluid">
                        </div>
                    </div>

                    <div class="product card border-0">
                        <div class="product-image">
                            <img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/img/product/kyle-loftus-590881-unsplash.5994ffa7.jpg" alt="White Tee" class="img-fluid">
                        </div>
                    </div>

                    <div class="product card border-0">
                        <div class="product-image">
                            <img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/img/product/serrah-galos-494231-unsplash-cropped.1eae57f5.jpg" alt="White Tee" class="img-fluid">
                        </div>
                    </div>

                    <div class="product card border-0">
                        <div class="product-image">
                            <img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/img/product/averie-woodard-319832-unsplash-cropped.756b7fb8.jpg" alt="White Tee" class="img-fluid">
                        </div>
                    </div>

                    <div class="product card border-0">
                        <div class="product-image">
                            <img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/img/product/dmitriy-ilkevich-437760-unsplash.6cf1669f.jpg" alt="White Tee" class="img-fluid">
                        </div>
                    </div>

                    <div class="product card border-0">
                        <div class="product-image">
                            <img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/img/product/serrah-galos-494279-unsplash.ef5e0861.jpg" alt="White Tee" class="img-fluid">
                        </div>
                    </div>

                    <div class="product card border-0">
                        <div class="product-image">
                            <img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/img/product/ian-dooley-347968-unsplash.d1de4678.jpg" alt="White Tee" class="img-fluid">
                        </div>
                    </div>

                    <div class="product card border-0">
                        <div class="product-image">
                            <img src="https://d19m59y37dris4.cloudfront.net/sell/1-2-5/img/product/alex-holyoake-571682-unsplash-cropped.8340f478.jpg" alt="White Tee" class="img-fluid">
                        </div>
                    </div>
                    <!-- /product masonry-->
                </div>
            </div>

        </div>
    </div>
</section>