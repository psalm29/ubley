
<?php

$sliders = slider::all();
$count = count($sliders);

if ($sliders) {
//    request::pre($sliders);
    ?>
<!-- Hero Section-->
<section class="home-full-slider-wrapper">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="height: 100%;">
        <ol class="carousel-indicators">
            <?php
            $sf = 0;
            foreach ($sliders as $slider) {
                ?>
            <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $sf; ?>" class="<?php
            if ($sf == 0) {
                echo 'active';
                $sf++;
            }
            ?>"></li>
            <?php
            }
            ?>

        </ol>
        <div class="carousel-inner" role="listbox" style="height: 100%;">
            <?php
$sd = 0;
            foreach ($sliders as $slider) {
                ?>
                <div class="carousel-item <?php
                if ($sd == 0) {
                    echo 'active';
                    $sd++;
                }
                ?>" style="height: 100%;">
                    <div style="background: #f8d5cf; height: 100%;" class="item home-full-item"><img src="<?php echo config::baseUploadSliderUrl().$slider['image']; ?>" alt="" class="bg-image">
                        <div class="container-fluid h-100 py-5">
                            <div class="row align-items-center h-100">
                                <div class="col-lg-8 col-xl-6 mx-auto text-white text-center">
                                    <h1 class="mb-5 display-2 font-weight-bold text-serif"><?php echo $slider['title']; ?></h1>
                                    <p class="lead mb-4"><?php echo $slider['description']; ?></p>
                                    <?php
                                    if ($slider['link']) {
                                        ?>
                                        <p> <a href="<?php echo $slider['link']; ?>" target="_blank" class="btn btn-light">Shop Now</a></p>
                                    <?php
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }

            ?>

        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<?php
}

$categories = category::all();

if ($categories) {
    $count = 0;
    ?>
    <section>
        <div class="container py-6">
            <div class="row">
                <div class="col-xl-8 mx-auto text-center mb-5">
                    <h2 class="text-uppercase">Shop Categories </h2>
                    <p class="lead text-muted">We have variety of categories for you to shop from</p>
                </div>
            </div>
            <div class="row">
                <?php

                category::home();

                ?>


            </div>
        </div>
    </section>
    <?php
}

$products = product::view(4);

if ($products) {
    ?>
    <section class="py-6 bg-gray-100">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 mx-auto text-center mb-5">
                    <h2 class="text-uppercase">Latest Products</h2>
<!--                    <p class="lead text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>-->
                </div>
            </div>
            <div class="row">
                <?php

                $products = product::view(8);

                if ($products) {
                    ?>

                    <?php

                    foreach ($products as $product) {
                        product::display($product, true);
                    }

                }

                ?>
            </div>
        </div>
    </section>
    <?php



}

$promo = promo::get();

if ($promo['status'] == 1) {
    ?>
    <section class="py-6 position-relative light-overlay"><img src="<?php echo config::baseUploadPromoUrl().$promo['image']; ?>" style="" alt="image" class="bg-image">
        <div class="container">
            <div class="overlay-content text-center text-dark">
                <p class="text-uppercase font-weight-bold mb-1 letter-spacing-5"><?php echo $promo['sub_title']; ?></p>
                <h3 class="display-1 font-weight-bold text-serif mb-4"><?php echo $promo['title']; ?></h3><a href="<?php echo $promo['link']; ?>" class="btn btn-dark">Shop Now</a>
            </div>
        </div>
    </section>
<?php
}

?>

